(function($) {
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
	function productFilter(){
		var checked = $('.filter-select:checkbox:checked');
		var filterClass = '';
		var _col = $('.product-item-col');
		if(checked.length > 0){
			_col.removeClass('show-filter').addClass('prepend-filter')
		}else{
			_col.removeClass('show-filter').removeClass('prepend-filter')
		}

		checked.each(function (index, value){
			// $(this).parent().children().addClass('red');
			filterClass += '.' + $(this).data('tax') + '-' +$(this).val();
			var _toFilter = $(filterClass);

			_toFilter.each(function (index, value){
				$(this).parent().addClass('show-filter');
			});
		});
	}
	function update_cart_count(){
		var count = $('#cart-count');
		count.html('<i class="far fa-smile-beam fa-spin" style="color: #4ce55e"></i>');

		jQuery.ajax({
			type: 'POST',
			url: '/wp-admin/admin-ajax.php',
			data: {
				action: 'update_cart_count',
			},
			success: function (results) {
				count.html(results/10);
			}
		});
	}
	function addToCartAnimation(button){
		var target        = $('#cart-count'),
			target_offset = target.offset();

		var target_x = target_offset.left,
			target_y = target_offset.top;

		var obj_id = 1 + Math.floor(Math.random() * 100000),
			obj_class = 'cart-animation-helper',
			obj_class_id = obj_class + '_' + obj_id;

		var obj = $("<div>", {
			'class': obj_class + ' ' + obj_class_id
		});

		button.parent().parent().append(obj);

		var obj_offset = obj.offset(),
			dist_x = target_x - obj_offset.left + 10,
			dist_y = target_y - obj_offset.top + 10,
			delay  = 0.8; // seconds

		setTimeout(function(){
			obj.css({
				'transition': 'transform ' + delay + 's ease-in',
				'transform' : "translateX(" + dist_x + "px)"
			});
			$('<style>.' + obj_class_id + ':after{ \
				transform: translateY(' + dist_y + 'px); \
				opacity: 1; \
				z-index: 99999999999; \
				border-radius: 100%; \
				height: 20px; \
				width: 20px; margin: 0; \
			}</style>').appendTo('head');
		}, 0);


		obj.show(1).delay((delay + 0.02) * 1000).hide(1, function() {
			$(obj).remove();
		});
	}
	$(document).on('click', '.plus, .minus', function () {

		var $_class = $(this).hasClass('mini-cart-ctrl') ? '.mini-cart-qty-for-' : '.qty-for-';
		var qty =  $($_class + $(this).data('id'));
		var val = parseFloat(qty.val());
		var max = 9999;
		var min = 1;
		var step = 1;

		if ($(this).is('.plus')) {
			if (max && (max <= val)) {
				qty.val(max);
			} else {
				qty.val(val + step);
			}
		} else {
			if (min && (min >= val)) {
				qty.val(min);
			} else if (val > 1) {
				qty.val(val - step);
			}
		}

		if($(this).hasClass('mini-cart-ctrl')){
			$('.preloader').addClass('now-loading');
			updateMiniCart($(this).data('key'), qty.val())
		}

	});
	$('body').on('click', '.remove', function (e) {
		// e.preventDefault();
		update_cart_count();
	});
	$( document ).ready(function() {
		$('.counter-number').rCounter({
			duration: 30
		});
		$('#get-details').click(function () {
			var idProdHidden = $('.prod-form-wrap').data('id');
			var linkProdHidden = $('.prod-form-wrap').data('link');
			$('#hidden-input-name').html(idProdHidden);
			$('#hidden-prod-link').html(linkProdHidden);
		});
		$('.add_to_cart_button').click(function (e) {
			e.preventDefault();
			addToCartAnimation($(this));
			var cartEl = $('.mini-cart-wrap');
			cartEl.html('<div class="cart-loading"><i class="fas fa-spinner fa-pulse"></i></div>');


			var btn = $(this);
			btn.addClass('loading');


			$(this).addClass('adding-cart');
			var product_id = $(this).data('id');
			var quantity = $('.qty-for-' + product_id).val();


			jQuery.ajax({
				url: '/wp-admin/admin-ajax.php',
				type: 'POST',
				data: {
					action: 'add_product_to_cart',
					product_id: product_id,
					quantity: quantity,
				},

				success: function (results) {
					btn.removeClass('loading').addClass('clicked');
					cartEl.html(results);
					update_cart_count();
				}
			});

		});

		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.slideFadeToggle();
			$(this).toggleClass('is-active');
		});
		$( '.rev-pop-trigger' ).click( function() {
			var contentAll = $( this ).children('.hidden-review').html();
			$( '#reviews-pop-wrapper' ).html( contentAll );
			$( '#reviewsModal' ).modal( 'show' );
		});
		$( '#reviewsModal' ).on( 'hidden.bs.modal', function( e ) {
			$( '#reviews-pop-wrapper' ).html( '' );
		});
		$('.cat-item-img').hover(function (){
			$(this).children('.cat-item-info').children('.cat-item-info-part').slideFadeToggle();
		});
		$('.home-cat-item').hover(function (){
			$(this).children('.home-cats-children').slideFadeToggle();
		});
		$('.search-trigger').click(function () {
			$('.pop-form-search').addClass('show-popup');
			$('.float-form-search').addClass('show-float-form');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-form-search').click(function () {
			$('.pop-form-search').removeClass('show-popup');
			$('.float-form-search').removeClass('show-float-form');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.pop-trigger').click(function () {
			$('.pop-form').addClass('show-popup');
			$('.float-form').addClass('show-float-form');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-form').click(function () {
			$('.pop-form').removeClass('show-popup');
			$('.float-form').removeClass('show-float-form');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$('.main-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 3000,
			rtl: true,
			arrows: false,
			dots: false,
		});
		$('.gallery-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			rtl: true,
			asNavFor: '.thumbs'
		});
		$('.thumbs').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			// rtl: true,
			asNavFor: '.gallery-slider',
			dots: true,
			centerPadding: 0,
			vertical: true,
			arrows: false,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				// {
				// 	breakpoint: 768,
				// 	settings: {
				// 		slidesToShow: 2,
				// 	}
				// },
			]
		});
		$('.product-slider').slick({
			slidesToShow: 7,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1920,
					settings: {
						slidesToShow: 6,
					}
				},
				{
					breakpoint: 1600,
					settings: {
						slidesToShow: 5,
					}
				},
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 4,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 500,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$('.partners-slider').slick({
			slidesToShow: 6,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1600,
					settings: {
						slidesToShow: 5,
					}
				},
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 4,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 2,
					}
				},
			]
		});
		$('.video-slider').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-video').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		var accordion = $('#accordion');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().addClass('active-faq');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().removeClass('active-faq');
		});
		var accordionCats = $('#accordion-cats');
		accordionCats.collapse({
			toggle: false
		});
		$('.accordion-trigger').on('click',function(){
			$(this).toggleClass('minus-acc');
			$(this).parent('.card-header').parent('.card').children('.body-acc').toggle('show');
			$(this).parent('.card-header').toggleClass('active-card');
		});
		//Socials
		$('.trigger-wrap').hover(function(){
			$(this).children('.all-socials').addClass('show-share');
		}, function(){
			$(this).children('.all-socials').removeClass('show-share');
		});
		$('#filter-select-submit').click(function() {
			productFilter();
		});
		$('#filter-reset-submit').click(function() {
			$('.filter-select:checkbox').removeAttr('checked');
			$('.product-item-col').each(function (index, value){
				$(this).addClass('show-filter');
			});
			// productFilter();
		});
	});

	$('.load-more-posts').click(function() {
		var termID = $(this).data('term');
		// var params = $('.take-json').html();
		var ids = '';
		var video = $(this).data('content');
		var page = $(this).data('page');

		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				// postType: postType,
				termID: termID,
				ids: ids,
				page: page,
				video: video,
				// taxType: taxType,
				action: 'get_more_function',
			},
			success: function (data) {
				if (!data.html) {
					$('.load-more-posts').addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});
	//More products
	var button = $( '#loadmore a' );
	var paged = button.data( 'paged' );
	var	maxPages = button.data( 'maxpages' );
	var textLoad = button.data('loading');
	var textLoadMore = button.data('load');

	button.click( function( event ) {

		event.preventDefault();
		var ids = '';
		$('.more-prod').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		$.ajax({
			type : 'POST',
			dataType: 'json',
			url: '/wp-admin/admin-ajax.php',
			data : {
				paged : paged,
				ids: ids,
				action : 'loadmore'
			},
			beforeSend : function() {
				button.text(textLoad);
			},
			success : function( data ){

				paged++;
				$('.put-here-prods').append(data.html);
				button.text(textLoadMore);

				if( paged === maxPages ) {
					button.remove();
				}
				if (!data.html) {
					$('.more-link').addClass('hide');
				}
			}

		});

	} );
})( jQuery );
