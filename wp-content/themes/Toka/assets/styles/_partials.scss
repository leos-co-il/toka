.faq {
  @include back-img;
  background-color: $gray-main;
  background-image: url("../img/faq-back.png");
  position: relative;
  padding: 100px 0;
  .question-header {
	border: 1px solid #ffffff !important;
	box-shadow: 0 2px 18px 1px rgba(22, 22, 22, 0.17);
	background-color: #ffffff;
	transition: background-color .5s;
	&.active-faq {
	  background-color: transparent;
	  transition: background-color .5s;
	  .q-icon {
		color: $green-main;
		transition: color .5s;
	  }
	  .q-title-body {
		color: #ffffff;
	  }
	}
  }
  .base-output  {
	h1, h2, h3, h4, h5, h6 {
	  font-weight: 400;
	}
  }
  .card {
	border: none !important;
  }
  .question-card {
	margin-bottom: 15px;
	background-color: transparent;
  }
}
.question-title {
  display: flex;
  align-items: center;
  border: none !important;
  background-color: transparent;
  width: 100%;
  min-height: size(60);
  padding: 0;
}
.q-title-body {
  @include mid-text;
  text-align: right;
  color: #1e1e1e;
  font-weight: 700;
  padding: size(30) size(70);
  transition: color .5s;
}
.answer-body {
  background-color: transparent;
  padding: 0 size(70) 33px 20px;
}
.q-icon {
  position: absolute;
  top: size(30);
  right: 0;
  min-height: size(60);
  padding: 0 size(25);
  min-width: size(70);
  color: $gray-main;
  font-size: size(75);
  line-height: size(75);
  font-weight: 700;
  font-style: normal;
  letter-spacing: normal;
  transition: color .5s;
}
//Slider
.arrows-slider {
  [dir="rtl"] .slick-prev, [dir="rtl"] .slick-next {
	background-position: center;
	background-repeat: no-repeat;
	background-size: contain;
	z-index: 33;
  }
  [dir="rtl"] .slick-next {
	background-image: url("../iconsall/arrow-w-left.png");
  }
  [dir="rtl"] .slick-prev {
	background-image: url("../iconsall/arrow-w-right.png");
  }
  .slick-prev:before, .slick-next:before {
	color: transparent;
  }
}
.videos-col, .slider-side-arrows {
  [dir="rtl"] .slick-prev, [dir="rtl"] .slick-next {
	width: size(25);
	height: size(42);
  }
  .video-slider {
	padding: 0 2rem;
  }
  [dir="rtl"] .slick-next {
	left: 0;
  }
  [dir="rtl"] .slick-prev {
	right: 0;
  }
}
.arrows-slider-logos {
  [dir="rtl"] .slick-prev, [dir="rtl"] .slick-next {
	width: size(12);
	height: size(21);
  }
  [dir="rtl"] .slick-next {
	background-image: url("../iconsall/arrow-dark-left.png");
  }
  [dir="rtl"] .slick-prev {
	background-image: url("../iconsall/arrow-dark-right.png");
  }
}
.arrows-slider-base {
  [dir="rtl"] .slick-prev, [dir="rtl"] .slick-next {
	width: size(14);
	height: size(23);
	top: unset;
	bottom: -2rem;
  }
  [dir="rtl"] .slick-next {
	left: 0;
  }
  [dir="rtl"] .slick-prev {
	right: unset;
	left: 4rem;
  }
}
.h-arrows-slider-base {
  [dir="rtl"] .slick-prev, [dir="rtl"] .slick-next {
	bottom: -3rem;
  }
  [dir="rtl"] .slick-next {
	right: 4rem;
	left: unset;
  }
  [dir="rtl"] .slick-prev {
	right: 0;
  }
}
.base-slider-block {
  position: relative;
  padding: size(100) 0 0 0;
}
.slider-img-col {
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  img {
	width: 100%;
  }
}
.side-title-slider {
  position: relative;
  //&:before {
	//content: '';
	//display: block;
	//background-color: $gray-main;
	//position: absolute;
	//right: 0;
	//width: 100%;
	//height: 100%;
	//top: 0;
  //}
}
.slider-col-content {
  padding-bottom: 3rem;
}
//Video modal
.video-modal {
  .modal {
	z-index: 9999999999999999999999;
  }
  .modal-body {
	position: relative;
	padding: 0;
  }
  .modal-content {
	background: none;
	border: none;
  }
  .modal-backdrop {
	z-index: 10;
  }
  .close {
	position: absolute !important;
	top: -50px;
	right: -50px;
	width: 50px;
	height: 50px;
  }
  .modal .close-icon {
	padding: 0;
	margin: 0;
	font-size: 3rem;
  }
  .modal-body{
	iframe{
	  width: 100%;
	  height: 500px;
	  position: relative;
	}
  }
}
//product card
.query-product-list {
  align-items: stretch;
  justify-content: center;
}
.product-item-col {
  margin-bottom: 30px;
}
.product-small-card {
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  height: 100%;
  border: 1px solid #ffffff;
  padding: 5px;
  background-color: transparent;
  .top {
	width: 100%;
	height: 220px;
	@include d-center;
	background-color: #ffffff;
	position: relative;
  }
  .bottom {
	position: relative;
	background-color: $gray-main;
	flex-grow: 1;
	min-height: 50px;
	padding: 8px 10%;
	.woocommerce-loop-product__link {
	  justify-content: flex-start;
	}
  }
  .woocommerce-loop-product__title, .articul-card {
	color: #ffffff;
	font-size: size(19);
	line-height: size(18);
	text-align: center;
	font-weight: 400;
  }
  .woocommerce-loop-product__title {
	font-weight: 700;
  }
  a.button.add_to_cart_button {
	position: absolute !important;
	left: 0;
	bottom: 0;
	z-index: 4;
	background-color: $gray-main !important;
	width: 31px !important;
	height: 31px !important;
	padding: 0;
	min-height: unset;
	border: none;
	min-width: unset;
	&:after {
	  content: '';
	  width: 100%;
	  height: 100%;
	  display: block;
	  background-image: url("../iconsall/basket.png");
	  background-position: center;
	  background-repeat: no-repeat;
	  background-size: 24px;
	}
	&.added:after {
	  content: "\e017";
	  color: $green-main;
	  background-image: none;
	  margin: 0;
	}
  }
  .woocommerce-loop-product__link {
	position: absolute;
	right: 0;
	width: 100%;
	height: 100%;
	z-index: 2;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	img {
	  max-height: 100%;
	}
  }
}
.site-main {
  background-color: $gray-main;
  background-image: url("../img/shop-back.png");
  @include back-img;
  padding-top: 80px;
}
.woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button {
  text-align: center;
  min-height: 37px;
  box-shadow: 0 1px 7px rgba(0, 0, 0, 0.5);
  border: 1px solid #ffffff;
  background-color: $green-main !important;
  border-radius: 0 !important;
  color: #010101 !important;
  font-size: size(27);
  font-weight: 700;
  font-style: normal;
  letter-spacing: normal;
  line-height: size(40);
  padding: 4px 10px;
  min-width: 250px;
  transition: transform .5s;
  &:hover {
	transform: scale(0.95, 0.95);
	transition: transform .5s;
  }
}
.qty-text, .short-desc-title {
  font-size: size(32);
  font-weight: 400;
  color: #ffffff;
}
.short-desc-title {
  font-weight: 700;
  margin-bottom: 15px;
}
.qty-wrap {
  overflow: hidden;
  display: flex;
  margin: 0 10px;
  input, .plus, .minus {
	width: 26px;
	height: 26px;
	flex: auto;
	cursor: pointer;
	font-size: size(32);
	font-weight: 400;
	color: #ffffff;
	background-color: transparent;
	box-shadow: 0 1px 7px rgba(0, 0, 0, 0.5);
	border: 1px solid #ffffff;
  }
  .plus, .minus{
	display: flex;
	align-items: center;
	justify-content: center;
	transition: all .5s;
  }
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
	/* display: none; <- Crashes Chrome on hover */
	-webkit-appearance: none;
	margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
  }

  input[type=number] {
	-moz-appearance:textfield; /* Firefox */
  }
}
.product {
  form.cart {
	display: flex;
	flex-direction: column;
	align-items: flex-start;
  }
}
.single_add_to_cart_button {
  display: flex !important;
  justify-content: center;
  align-items: center;
  img {
	margin: 0 6px;
  }
}
.woocommerce div.product .product_title {
  font-size: size(45);
  font-weight: 700;
  line-height: size(49);
  color: #ffffff;
}
.gallery-slider-wrap {
  display: flex;
  .slick-dotted.slick-slider {
	margin-bottom: 0;
  }
  .slick-dots {
	bottom: unset;
	top: calc(100% + 10px);
	width: auto;
	display: flex;
	justify-content: flex-start;
	align-items: center;
	flex-wrap: wrap;
  }
  .slick-dots li button:before {
	color: $green-main;
  }
}
.gallery-slider {
  flex: 0 0 75%;
  max-width: 75%;
  &.slick-slider .slick-track, &.slick-slider .slick-list {
	height: 100%;
  }
}
.thumbs {
  flex: 0 0 25%;
  max-width: 25%;
  &.slick-slider .slick-track, &.slick-slider .slick-list {
	//height: 100% !important;
  }
}
.big-slider-item {
  background-size: 75%;
  background-position: center;
  background-repeat: no-repeat;
  width: 100%;
  display: flex;
  height: 100%;
}
.big-slider-item, .thumb-item {
  background-color: #ffffff;
}
.thumb-item {
  @include d-center;
}
.cart-animation-helper {
  margin: 0 20%;
  width: 0;
  height: 0;
  z-index: 999999999999999;
  position: absolute;
  &:after{
	opacity: 0;
	border-radius: 0;
	max-height: 19px;
	max-width: 19px;
	content: '';
	position: absolute;
	bottom: 0;
	left: 0;
	right: 0;
	margin: 0 auto;
	display: block;
	height: 19px;
	width: 19px;
	background-color: #ffffff;

	transition:
			transform 0.8s ease-out,
			margin 0.8s ease-out,
			opacity 0.8s ease-out,
			border-radius 0.4s ease-out,
			max-height 0.4s ease-out,
			max-width 0.4s ease-out;
  }
}
.woocommerce-message::before {
  color: $green-main;
}
.woocommerce-message {
  border-top-color: $green-main;
}
.woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button {
  background-color: $green-main;
  color: $gray-main !important;
}
.woocommerce-error, .woocommerce-info, .woocommerce-message {
  margin: 0;
  border-top-color: $green-main;
  background-color: $gray-main;
  &:before {
	color: $green-main;
  }
}
.return-to-shop {
  margin-top: 2rem;
}
.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt {
  background-color: $green-main;
}
.woocommerce table.shop_table td {
  border-top: 1px solid rgba(255, 255, 255);
}
.woocommerce table.shop_table {
  border: 1px solid rgba(255, 255, 255);
}
.cart-page-body {
  input[type="number"] {
	background-color: transparent;
  }
}
#add_payment_method #payment, .woocommerce-cart #payment, .woocommerce-checkout #payment {
  background: $gray-main;
}

//Post card
.post-card {
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-end;
  height: 100%;
  width: 100%;
  padding: 0 6%;
}
.col-post {
  margin-bottom: 28px;
}
.post-card-content {
  @include overlay;
  padding: 40% size(19) size(15) size(19);
  background-color: rgba(33, 30, 30, 0.55);
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
  overflow-y: auto;
  .base-text {
	font-size: 14px;
	line-height: 18px;
	color: #ffffff;
	font-weight: 300;
	text-align: center;
  }
}
.post-img {
  position: relative;
  display: flex;
  width: 100%;
  background-color: $gray-main;
  @include back-img;
  margin-bottom: 10px;
  &:after {
	content: '';
	display: block;
	padding-bottom: 86%;
  }
}
.post-card-title {
  @include mid-text;
  line-height: size(28);
  text-align: center;
  transition: color .5s;
  &:hover {
	color: $green-main;
	transition: color .5s;
  }
}
.post-link {
  font-size: size(30);
  line-height: size(32);
  color: #ffffff;
  font-weight: 700;
  background-image: url("../iconsall/arrow-link-left.png");
  background-position: 3px center;
  background-repeat: no-repeat;
  background-size: size(28);
  padding-left: 35px;
  transition: background-position .5s;
  &:hover {
	color: #ffffff;
	background-position: left center;
	transition: background-position .5s;
  }
}
//Sidebar
.sidebar-block {
  .card {
	background-color: transparent;
	border: none;
	border-radius: 0;
	margin-bottom: 15px;
  }
  .card-header {
	position: relative;
	padding: 0 0 0 size(45);
	border: none;
	background-color: #ffffff;
	border-radius: 0 !important;
	display: flex;
	justify-content: space-between;
	align-items: flex-start;
  }
  .accordion-trigger {
	position: absolute;
	left: 0;
	top: 0;
	width: size(40);
	height: size(40);
	border-radius: 0 !important;
	@include d-center;
	background-color: $gray-main;
	transition: background-color .5s;
	&:hover {
	  background-color: $green-main;
	  transition: background-color .5s;
	}
	&.minus-acc {
	  content: url("../iconsall/minus.png");
	}
  }
  .plus-icon-acc {
	content: url("../iconsall/plus.png");
  }
  .prod-cat-title {
	flex-grow: 1;
	padding: 3px 30px;
	display: flex;
	align-items: center;
	justify-content: flex-start;
	color: $gray-main;
	font-size: size(30);
	font-weight: 400;
	font-style: normal;
	letter-spacing: normal;
	line-height: size(32);
	transition: color .5s;
	&:hover {
	  color: $green-main;
	  transition: color .5s;
	}
  }
  .box-list label {
	@include mid-text;
	font-weight: 400;
	margin: 0 8px 0 0;
  }
  .card-body {
	padding: 14px 0 14px size(40);
  }
}
.product-item-col {
  display: none;
  &.show-filter{
	display: block !important;
  }
}
.inputs-wrapper{
  padding-left: size(40);
  display: flex;
  justify-content: space-between;
  align-items: stretch;
  input {
	width: calc(50% - 6px);
  }
  .button-send {
	margin-left: 6px;
	color: $green-main;
	border: 1px solid $green-main;
	&:hover {
	  background-color: $green-main;
	  color: $gray-main;
	}
  }
  .button-reset {
	margin-right: 6px;
	color: #fff;
	border: 1px solid #ffffff;
	&:hover {
	  background-color: #ffffff;
	  color: $gray-main;
	}
  }
}
.button-send, .button-reset {
  @include d-center;
  text-align: center;
  background-color: transparent;
  padding: 4px 20px;
  font-size: size(23);
  font-weight: 400;
  font-style: normal;
  letter-spacing: normal;
  line-height: size(25);
  transition: color, background-color .5s;
  &:hover {
	transition: color, background-color .5s;
  }
}
.check-li {
  display: flex;
  align-items: center;
  justify-content: flex-start;
}
.woocommerce-error, .woocommerce-info, .woocommerce-message {
  color: #fff;
}
