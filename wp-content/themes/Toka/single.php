<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
?>

<article class="page-body">
	<?php if ( function_exists('yoast_breadcrumb')) : ?>
		<div class="container-fluid pt-2 mb-4">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="d-flex justify-content-start">
		<div class="side-title side-title-post">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-11 col-12 post-content-col">
				<?php if (has_post_thumbnail()) : ?>
					<div class="post-image-wrap">
						<img src="<?= postThumb(); ?>" alt="post-image">
					</div>
				<?php endif; ?>
				<div class="base-output post-output">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<div class="granit-form granit-form-post m-0">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 4,
	'post_type' => 'post',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 4,
		'orderby' => 'rand',
		'post_type' => 'post',
		'post__not_in' => array($postId),
	]);
}
$link_all = $fields['all_posts_link'] ? $fields['all_posts_link'] : opt('blog_link');
if ($samePosts) : ?>
	<section class="home-posts homepage-body py-5 same-posts">
		<div class="side-title side-title-about">
			<?= $fields['same_title'] ? $fields['same_title'] :
				lang_text(['he' => 'עוד מאמרים שעשויים לעניין אתכם', 'en' => 'More articles that might interest you'], 'he'); ?>
		</div>
		<div class="container">
			<div class="row justify-content-center align-items-stretch put-here-posts">
				<?php foreach ($samePosts as $i => $post) {
					get_template_part('views/partials/card', 'post', [
						'post' => $post,
					]);
				} ?>
			</div>
			<?php if ($link_all) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<a href="<?= $link_all['url']; ?>" class="base-link">
							<?php $about_link = lang_text(['he' => ' לכל המאמרים', 'en' => 'To all articles'], 'he');
							echo (isset($link_all['title']) && $link_all['title']) ?
								$link_all['title'] : $about_link; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
endif;
get_footer(); ?>
