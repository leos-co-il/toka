<?php
/*
Template Name: קטגוריות
*/

get_header();
$fields = get_fields();
$cats = get_terms([
	'taxonomy' => 'product_cat',
	'hide_empty' => false,
	'parent' => 0,
]);

?>
<article class="page-body pb-0">
	<?php if ( function_exists('yoast_breadcrumb')) : ?>
		<div class="container-fluid pt-2 mb-4">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container pb-4">
		<div class="row justify-content-between">
			<div class="col">
				<div class="base-output text-center post-output cats-text-output">
<!--					<h1 class="form-title text-center">--><?php //the_title(); ?><!--</h1>-->
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($cats) : ?>
		<div class="cats-page mb-5">
			<div class="container">
				<div class="row justify-content-start align-items-stretch">
					<?php foreach($cats as $number => $parent_term) :
						$thumbnail_id = get_term_meta($parent_term->term_id, 'thumbnail_id', true );
						$cat_image = wp_get_attachment_url( $thumbnail_id ); ?>
						<div class="col-xl-4 col-sm-6 col-12 main-cat-col">
							<a class="main-cat-item" href="<?= get_term_link($parent_term); ?>">
								<span class="main-cat-img" <?php if ($cat_image) : ?>
									style="background-image: url('<?= $cat_image; ?>')"
								<?php endif; ?>></span>
								<span class="h-cat-name">
									<?= $parent_term->name; ?>
								</span>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="m-0">
		<?php get_template_part('views/partials/repeat', 'form'); ?>
	</div>
	<?php if ($fields['single_slider_seo']) {
		get_template_part('views/partials/content', 'slider',
			[
				'content' => $fields['single_slider_seo'],
				'img' => $fields['slider_img'],
			]);
	} ?>
</article>
<?php
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
endif;
get_footer(); ?>
