<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$tel_2 = opt('tel_2');
?>
<article class="page-body contact-page">
	<?php if ( function_exists('yoast_breadcrumb')) : ?>
		<div class="container-fluid pt-2 mb-4">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container">
		<div class="row justify-content-center contact-content-row">
			<div class="col-12">
				<?php get_template_part('views/partials/card', 'form_title',
						[
								'title' => $fields['contact_form_title'],
								'subtitle' => $fields['contact_form_subtitle'],
						]);
				?>
			</div>
			<div class="col-xl-9 col-lg-10 col-12">
				<div class="base-output contact-page-output">
					<?php the_content(); ?>
				</div>
				<div class="form wow zoomIn">
					<?php getForm('110'); ?>
				</div>
			</div>
			<div class="col-xl-9 col-lg-10 col-12">
				<ul class="contact-list row justify-content-lg-between justify-content-center align-items-start">
					<?php if ($address = opt('address')) : ?>
						<li class="contact-item col-lg-auto col-12">
							<a href="https://waze.com/ul?q=<?= $address; ?>"
							   class="contact-info-footer" target="_blank">
								<span class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-geo.png">
								</span>
								<span><?= $address; ?></span>
							</a>
						</li>
					<?php endif;
					if ($mail = opt('mail')) : ?>
						<li class="contact-item col-lg-auto col-12">
							<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
								<span class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-mail.png">
								</span>
								<span><?= $mail; ?></span>
							</a>
						</li>
					<?php endif;
					if ($tel || $tel_2) : ?>
						<li class="contact-item col-lg-auto col-12">
							<a href="tel:<?= $tel ? $tel : $tel_2; ?>" class="contact-info-footer">
								<span class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-tel.png">
								</span>
								<span><?= $tel.'|'; ?></span>
								<span><?= $tel_2; ?></span>
							</a>
						</li>
					<?php endif; ?>
				</ul>

			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
