<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$posts = new WP_Query([
	'posts_per_page' => 8,
	'post_type' => 'post',
	'suppress_filters' => false,
]);
$published_posts = count(get_posts([
	'numberposts' => -1,
	'post_type' => 'post',
	'suppress_filters' => false
]));
?>

<article class="article-page-body page-body">
	<?php if ( function_exists('yoast_breadcrumb')) : ?>
		<div class="container-fluid pt-2 mb-4">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10 col-12">
				<h1 class="base-title text-center mb-3"><?php the_title(); ?></h1>
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($posts->have_posts()) : ?>
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12">
					<div class="row justify-content-center align-items-stretch put-here-posts">
						<?php foreach ($posts->posts as $i => $post) {
							get_template_part('views/partials/card', 'post', [
									'post' => $post,
							]);
						} ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif;
	if ($published_posts && $published_posts > 8) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="more-link load-more-posts">
						<?= lang_text(['he' => 'עוד מאמרים', 'en' => 'More posts'], 'he'); ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="pt-4">
		<?php get_template_part('views/partials/repeat', 'form'); ?>
	</div>
</article>
<?php
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
		]);
}
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
endif;
get_footer(); ?>
