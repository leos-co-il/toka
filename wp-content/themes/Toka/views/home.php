<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();
if ($fields['main_slider']) : ?>
	<section class="main-block-home">
		<div class="main-slider" dir="rtl">
			<?php foreach ($fields['main_slider'] as $slide) : ?>
				<div class="main-slide" <?php if ($slide['main_img']) : ?>
					style="background-image: url('<?= $slide['main_img']['url']; ?>')"<?php endif; ?>>
					<div class="container content-slide-in">
						<div class="row justify-content-center">
							<div class="col-12 d-flex flex-column justify-content-center align-items-center">
								<h2 class="main-title"><?= $slide['main_title']; ?></h2>
								<h2 class="main-subtitle"><?= $slide['main_subtitle']; ?></h2>
								<?php if ($slide['h_main_link']) : ?>
									<a href="<?= $slide['h_main_link']['url']; ?>" class="main-link">
										<?= lang_text(['he' => 'הקליקו ליצירת קשר', 'en' => 'Click to contact'], 'he') ?>
									</a>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</section>
<?php endif;
if ($fields['clients_slider']) : ?>
<section class="clients-block">
	<div class="container clients-block-title">
		<div class="row justify-content-center">
			<div class="col-auto">
				<h2 class="clients-title">
					<?= $fields['h_clients_title'] ? $fields['h_clients_title'] :
							lang_text(['he' => 'מבין לקוחותינו/', 'en' => 'Our customers /'], 'he'); ?>
				</h2>
			</div>
		</div>
	</div>
	<div class="clients-line arrows-slider">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-11 arrows-slider-logos">
					<div class="partners-slider" dir="rtl">
						<?php foreach ($fields['clients_slider'] as $partner) : ?>
							<div>
								<div class="client-logo">
									<img src="<?= $partner['url']; ?>" alt="customer-logo">
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>
<div class="homepage-body pt-90">
	<?php if ($fields['h_about_text']) : ?>
		<section class="home-about-block home-posts">
			<?php if ($fields['h_about_title']) : ?>
				<div class="side-title side-title-about mb-3">
					<div class="form-title-wrap">
						<h2 class="form-title"><?= $fields['h_about_title']; ?></h2>
						<?php if ($logo_text = opt('logo_text')) : ?>
							<div><img src="<?= $logo_text['url']; ?>" alt="logo-text"></div>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
			<div class="container">
				<div class="row justify-content-between align-items-center">
					<div class="col-lg-6 col-md-10 col-12 d-flex flex-column align-items-start">
						<div class="base-output mb-3">
							<?= $fields['h_about_text']; ?>
						</div>
						<?php if ($fields['h_about_link']) : ?>
							<a href="<?= $fields['h_about_link']['url'];?>" class="post-link">
								<?= (isset($fields['h_about_link']['title']) && $fields['h_about_link']['title'])
										? $fields['h_about_link']['title'] : lang_text(['he' => 'קראו עוד', 'en' => 'Read more'], 'he');
								?>
							</a>
						<?php endif; ?>
					</div>
					<?php if($fields['h_about_img']) : ?>
						<div class="col-xl-5 col-lg-6 about-img-wrap">
							<img src="<?= $fields['h_about_img']['url']; ?>" alt="about-img">
						</div>
					<?php endif; ?>
				</div>
			</div>
		</section>
	<?php endif;
	if ($fields['h_counter']) : ?>
		<div class="counter-block-wrap">
			<div class="container">
				<div class="row justify-content-center" id="start-count">
					<?php foreach ($fields['h_counter'] as $count) : ?>
						<div class="col-lg-3 col-sm-6 col-12 counter-block">
							<div class="counter-item wow fadeIn">
								<div class="counter-number-wrap">
									<div class="counter-number counter-num" data-from="1"
										 data-to="<?= $count['number']; ?>" data-speed="1500">
										<?= $count['number']; ?>
									</div>
								</div>
								<h4 class="counter-title"><?= $count['title']; ?></h4>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif;
	if ($fields['h_prod_slider']) : ?>
		<div class="container-fluid arrows-slider pb-5">
			<div class="row justify-content-center">
				<div class="col-11">
					<?php if ($fields['h_prod_slider_text']) : ?>
						<div class="base-output block-output text-center">
							<?= $fields['h_prod_slider_text']; ?>
						</div>
					<?php endif; ?>
					<div class="slider-side-arrows">
						<div class="product-slider" dir="rtl">
							<?php foreach ($fields['h_prod_slider'] as $_p){
								echo '<div class="slide-item-prod p-3">';
								setup_postdata($GLOBALS['post'] =& $_p);
								wc_get_template_part( 'content', 'product' );
								echo '</div>';
							}
							wp_reset_postdata();?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>
<?php get_template_part('views/partials/repeat', 'form'); ?>
<?php if ($fields['h_cats']) : ?>
	<section class="cats-block wow zoomIn" data-wow-delay="0.2s">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<?php if ($fields['h_cats_title']) : ?>
						<h2 class="cats-block-title"><?= $fields['h_cats_title']; ?></h2>
					<?php endif;
					if ($fields['h_cats_subtitle']) : ?>
						<h3 class="cats-block-subtitle"><?= $fields['h_cats_subtitle']; ?></h3>
					<?php endif; ?>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach($fields['h_cats'] as $number => $parent_term) : ?>
					<div class="home-cat-col col-xl-4 col-sm-6 col-12">
						<?php $term_children = get_term_children($parent_term->term_id, 'product_cat'); ?>
						<div class="home-cat-item">
							<a class="h-cat-name" href="<?= get_term_link($parent_term); ?>">
								<?= $parent_term->name; ?>
							</a>
							<?php if ($term_children) : ?>
								<div class="home-cats-children">
									<ul class="cats-list">
										<?php foreach ($term_children as $x => $child) : $cat_child_item = get_term_by( 'id', $child, 'product_cat' ); ?>
											<li class="mb-1">
												<a href="<?= get_term_link($cat_child_item); ?>" class="home-child-cat">
													<?= $cat_child_item->name ?>
												</a>
											</li>
										<?php endforeach; ?>
									</ul>
								</div>
							<?php endif; ?>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif; ?>
<div class="homepage-body pt-5">
	<?php if ($fields['h_video_slider']) : ?>
		<div class="home-posts">
			<?php if ($fields['h_video_block_title']) : ?>
				<div class="side-title">
					<?= $fields['h_video_block_title']; ?>
				</div>
			<?php endif; ?>
		</div>
		<div class="videos-col arrows-slider">
			<div class="container">
				<div class="row justify-content-center align-items-stretch">
					<div class="col-12">
						<div class="video-slider" dir="rtl">
							<?php foreach ($fields['h_video_slider'] as $video) : ?>
								<div class="mb-3 p-2">
									<div class="video-item" style="background-image: url('<?= getYoutubeThumb($video['video_link'])?>')">
									<span class="play-video" data-video="<?= getYoutubeId($video['video_link']); ?>">
										<img src="<?= ICONS ?>play.png" alt="play">
									</span>
									</div>
									<h4 class="video-title"><?= $video['video_title']; ?></h4>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="video-modal">
			<div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
				 aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-body" id="iframe-wrapper"></div>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="close-icon">×</span>
						</button>
					</div>
				</div>
			</div>
		</div>
	<?php endif;
	if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
		]);
	} ?>
</div>
<div class="granit-form m-0">
	<div class="repeat-form-block">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12">
					<div class="form-title-wrap">
						<?php if ($fields['h_base_form_title']) : ?>
							<h2 class="form-title"><?= $fields['h_base_form_title']; ?></h2>
						<?php endif;
						if ($logo_text = opt('logo_text')) : ?>
							<div><img src="<?= $logo_text['url']; ?>" alt="logo-text"></div>
						<?php endif; ?>
					</div>
					<?php if ($fields['h_base_form_subtitle']) : ?>
						<h3 class="form-subtitle"><?= $fields['h_base_form_subtitle']; ?></h3>
					<?php endif; ?>
				</div>
				<div class="col-12">
					<div class="base-form-wrap">
						<?php getForm('113'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="homepage-body pt-5">
	<?php if ($fields['h_posts']) : ?>
		<section class="home-posts pb-140">
			<?php if ($fields['h_posts_title']) : ?>
				<div class="side-title">
					<?= $fields['h_posts_title']; ?>
				</div>
			<?php endif; ?>
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-xl-11 col-12">
						<div class="row justify-content-center align-items-stretch put-here-posts">
							<?php foreach ($fields['h_posts'] as $i => $post) {
								get_template_part('views/partials/card', 'post', [
										'post' => $post,
								]);
							} ?>
						</div>
						<?php if ($fields['h_posts_link']) : ?>
							<div class="row justify-content-center">
								<div class="col-auto">
									<a href="<?= $fields['h_posts_link']['url']; ?>" class="base-link">
										<?php $about_link = lang_text(['he' => ' לכל המאמרים', 'en' => 'To all articles'], 'he');
										echo (isset($fields['h_posts_link']['title']) && $fields['h_posts_link']['title']) ?
												$fields['h_posts_link']['title'] : $about_link; ?>
									</a>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
	<?php endif;
	if ($fields['h_slider_seo']) :
		$slider_img = $fields['h_slider_img']; ?>
		<div class="base-slider-block arrows-slider arrows-slider-base h-arrows-slider-base home-slider-wrap"
		<?php if ($slider_img) : ?>
			style="background-image: url('<?= $slider_img['url']; ?>')"
		<?php endif; ?>>
			<div class="brown-back">
			</div>
			<div class="container-fluid">
				<div class="row justify-content-center align-items-center">
					<div class="col-xl-11 col-12">
						<div class="row justify-content-between align-items-center">
							<div class="<?= $slider_img ? 'col-xl-5 col-lg-6 col-12' : 'col-12'; ?> slider-col-content">
								<div class="base-slider" dir="rtl">
									<?php foreach ($fields['h_slider_seo'] as $content) : ?>
										<div class="slider-base-item">
											<div class="d-flex flex-column align-items-start pt-5">
												<?php if ($content['h_slider_title']) : ?>
													<div class="h-slider-title">
														<?= $content['h_slider_title']; ?>
													</div>
												<?php endif; ?>
												<div class="base-output slider-output">
													<?= $content['content']; ?>
												</div>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
							<?php if ($slider_img) : ?>
								<div class="col-lg-6 col-12 slider-img-col">
									<img src="<?= $slider_img['url']; ?>" class="img-slider">
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>
<?php if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
			]);
endif;
get_footer(); ?>
