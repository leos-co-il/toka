<?php
/*
Template Name: אודות
*/

get_header();
$fields = get_fields();


?>

<article class="page-body pb-0">
	<?php if ( function_exists('yoast_breadcrumb')) : ?>
		<div class="container-fluid pt-2 mb-4">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="d-flex justify-content-start">
		<div class="side-title side-title-about mb-3">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
	<div class="container pb-4">
		<div class="row justify-content-between">
			<div class="<?= has_post_thumbnail() ? 'col-lg-6 col-12' : 'col-12'; ?>">
				<div class="base-output post-output">
					<?php the_content(); ?>
				</div>
			</div>
			<?php if (has_post_thumbnail()) : ?>
				<div class="col-xl-5 col-lg-6 col-12">
					<img src="<?= postThumb(); ?>" alt="post-image">
				</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="m-0">
		<?php get_template_part('views/partials/repeat', 'form'); ?>
	</div>
	<?php if ($fields['single_slider_seo']) {
		get_template_part('views/partials/content', 'slider',
			[
				'content' => $fields['single_slider_seo'],
				'img' => $fields['slider_img'],
			]);
	} ?>
</article>
<?php
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
endif;
get_footer(); ?>
