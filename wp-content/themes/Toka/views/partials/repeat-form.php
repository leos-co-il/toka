<div class="repeat-form-block">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 dotted-form">
				<?php get_template_part('views/partials/card', 'form_title',
					[
						'title' => (isset($args['title']) && $args['title']) ? $args['title'] : opt('base_form_title'),
						'subtitle' => (isset($args['subtitle']) && $args['subtitle']) ? $args['subtitle'] : opt('base_form_subtitle'),
					]);
				?>
			</div>
			<div class="col-12">
				<div class="base-form-wrap">
					<?php getForm('105'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
