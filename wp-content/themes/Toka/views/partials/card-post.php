<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-xl-3 col-md-6 col-sm-10 col-12 col-post">
		<div class="post-card more-card" data-id="<?= $args['post']->ID; ?>">
			<div class="post-img"<?php if (has_post_thumbnail($args['post'])) : ?>
				style="background-image: url('<?= postThumb($args['post']); ?>')" <?php endif; ?>>
				<a class="post-card-content" href="<?= $link; ?>">
					<span class="post-card-title"><?= $args['post']->post_title; ?></span>
					<span class="base-text">
					<?= text_preview($args['post']->post_content, 10); ?>
					</span>
				</a>
			</div>
			<a href="<?= $link; ?>" class="post-link">
				<?= lang_text(['he' => 'קראו עוד', 'en' => 'Read more'], 'he'); ?>
			</a>
		</div>
	</div>
<?php endif; ?>
