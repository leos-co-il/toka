<div class="dots-line mb-4">
	<span class="dot-item"></span>
	<span class="dot-item"></span>
	<span class="dot-item"></span>
	<span class="dot-item"></span>
	<span class="dot-item"></span>
</div>
<div class="form-title-wrap">
	<?php if (isset($args['title']) && $args['title']) : ?>
		<h2 class="form-title"><?= $args['title']; ?></h2>
	<?php endif;
	if ($logo_text = opt('logo_text')) : ?>
		<div><img src="<?= $logo_text['url']; ?>" alt="logo-text"></div>
	<?php endif; ?>
</div>
<?php if (isset($args['subtitle']) && $args['subtitle']) : ?>
	<h3 class="form-subtitle"><?= $args['subtitle']; ?></h3>
<?php endif; ?>
