<?php
$facebook = opt('facebook');
$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$tel_2 = opt('tel_2');
?>

<footer>
	<div class="footer-main">
		<a id="go-top">
			<img src="<?= ICONS ?>to-top.png" alt="to-top">
			<span class="top-text">
				<?= lang_text(['he' => 'חזרה למעלה', 'en' => 'Back to top'], 'he'); ?>
			</span>
		</a>
		<div class="foo-form-wrap">
			<div class="container">
				<div class="row justify-content-lg-between justify-content-center align-items-center mb-5">
					<div class="col-auto">
						<?php if ($f_title = opt('foo_form_title')) : ?>
							<h2 class="form-title mb-3"><?= $f_title; ?></h2>
						<?php endif;
						if ($f_text = opt('foo_form_subtitle')) : ?>
							<h3 class="foo-form-subtitle"><?= $f_text; ?></h3>
						<?php endif; ?>
					</div>
					<?php if ($logo = opt('logo')) : ?>
						<div class="col-xl-3 col-lg-4 col-sm-6 col-8 foo-logo-col">
							<a href="/" class="d-flex justify-content-lg-end justify-content-center align-items-center w-100 h-100">
								<img src="<?= $logo['url'] ?>" alt="logo" class="w-100">
							</a>
						</div>
					<?php endif; ?>
				</div>
				<div class="row justify-content-center">
					<div class="col-12">
						<div class="rep-form-col">
							<?php getForm('107'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container mt-5">
			<div class="row justify-content-sm-between justify-content-center">
				<div class="col-lg col-sm-6 col-11 foo-menu main-footer-menu">
					<h3 class="foo-title">
						<?= lang_text(['he' => 'ניווט מהיר', 'en' => 'Fast navigation'], 'he'); ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-menu', '1', '',
								'main_menu h-100'); ?>
					</div>
				</div>
				<div class="col-lg col-sm-6 col-11 foo-menu prods-footer-menu">
					<h3 class="foo-title">
						<?= lang_text(['he' => 'המוצרים שלנו', 'en' => 'Our products'], 'he'); ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-products-menu', '1'); ?>
					</div>
				</div>
				<div class="col-lg col-sm-6 col-11 foo-menu links-footer-menu">
					<h3 class="foo-title">
						<?php $title_m = opt('foo_links_menu_title');
						echo $title_m ? $title_m : lang_text(['he' => 'מאמרים חשובים', 'en' => 'Important articles'], 'he'); ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-links-menu', '1'); ?>
					</div>
				</div>
				<div class="col-lg col-sm-6 col-11 foo-menu contacts-footer-menu">
					<h3 class="foo-title">
						<?= lang_text(['he' => 'בואו נשאר בקשר', 'en' => "Let's stay in touch"], 'he'); ?>
					</h3>
					<div class="menu-border-top contact-menu-foo mb-4">
						<ul class="contact-list d-flex flex-column">
							<?php if ($address) : ?>
								<li>
									<a href="https://waze.com/ul?q=<?= $address; ?>"
									   class="contact-info-footer" target="_blank">
										<img src="<?= ICONS ?>foo-geo.png">
										<span>
											<?= $address; ?>
										</span>
									</a>
								</li>
							<?php endif; ?>
							<?php if ($mail) : ?>
								<li>
									<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
										<img src="<?= ICONS ?>foo-mail.png">
										<?= $mail; ?>
									</a>
								</li>
							<?php endif;
							if ($tel || $tel_2) : ?>
								<li>
									<a href="tel:<?= $tel ? $tel : $tel_2; ?>" class="contact-info-footer">
										<img src="<?= ICONS ?>foo-tel.png">
										<span><?= $tel.'|'; ?></span>
										<span><?= $tel_2; ?></span>
									</a>
								</li>
							<?php endif; ?>
						</ul>
					</div>
					<?php if ($facebook) : ?>
						<div class="facebook-widget">
							<h3 class="foo-title">
								<?= lang_text(['he' => 'עקבו אחרינו בפייסבוק', 'en' => 'Follow us'], 'he'); ?>
							</h3>
							<div class="menu-border-top">
								<iframe src="https://www.facebook.com/plugins/page.php?href=<?= $facebook; ?>&tabs=timeline&width=300px&height=200px&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId"
										width="250px" height="150px" style="border:none;overflow:hidden" scrolling="no"
										frameborder="0" allowTransparency="true" allow="encrypted-media">
								</iframe>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($suppliers = opt('suppliers')) : ?>
		<section class="clients-block">
			<div class="container clients-block-title">
				<div class="row justify-content-center">
					<div class="col-auto">
						<h2 class="clients-title">
							<?= lang_text(['he' => 'הספקים שלנו/', 'en' => 'Our suppliers /'], 'he'); ?>
						</h2>
					</div>
				</div>
			</div>
			<div class="clients-line arrows-slider">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12 arrows-slider-logos">
							<div class="partners-slider" dir="rtl">
								<?php foreach ($suppliers as $partner) : ?>
									<div>
										<div class="client-logo">
											<img src="<?= $partner['url']; ?>" alt="customer-logo">
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
