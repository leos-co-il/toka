<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php wp_head(); ?>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
	<script>
		const timerStart = Date.now();
	</script>
	<div class="debug bg-danger border">
		<p class="width">
			<span>Width:</span>
			<span class="val"></span>
		</p>
		<p class="height">
			<span>Height:</span>
			<span class="val"></span>
		</p>
		<p class="media-query">
			<span>Media Query:</span>
			<span class="val"></span>
		</p>
		<p class="zoom">
			<span>Zoom:</span>
			<span class="val"></span>
		</p>
		<p class="dom-ready">
			<span>DOM Ready:</span>
			<span class="val"></span>
		</p>
		<p class="load-time">
			<span>Loading Time:</span>
			<span class="val"></span>
		</p>
	</div>
<?php endif; ?>


<header>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-xl-11 col-12">
				<div class="row align-items-center justify-content-between position-relative">
					<div class="drop-menu">
						<nav class="header-nav">
							<?php getMenu('dropdown-menu', '1'); ?>
						</nav>
					</div>
					<div class="col-auto">
						<button class="hamburger hamburger--spin menu-trigger" type="button">
							<span class="hamburger-box">
								<span class="hamburger-inner"></span>
							</span>
						</button>
					</div>
					<div class="col-md col-none">
						<nav id="MainNav" class="h-100">
							<div id="MobNavBtn">
								<span></span>
								<span></span>
								<span></span>
							</div>
							<?php getMenu('header-menu', '1', '', 'main_menu h-100'); ?>
						</nav>
					</div>
					<div class="col-auto">
						<div class="row justify-content-end align-items-center row-header-custom">
							<div class="col-auto d-flex align-items-center">
								<a href="<?= get_permalink(get_option('woocommerce_myaccount_page_id')); ?>" class="login-link-header">
									<?= lang_text(['he' => 'התחברו', 'en' => 'Log in'], 'he'); ?>
								</a>
								<a href="<?= wc_get_cart_url(); ?>" class="header-btn" id="mini-cart">
									<img src="<?= ICONS ?>basket.png" alt="shopping-cart">
									<span class="count-circle" id="cart-count">
										<?= WC()->cart->get_cart_contents_count(); ?>
									</span>
								</a>
							</div>
							<?php if ($tel = opt('tel')) : ?>
								<div class="col-auto tel-col">
									<a href="tel:<?= $tel; ?>" class="header-tel">
										<img src="<?= ICONS ?>header-tel.png" alt="tel">
										<span class="header-tel-number"><?= $tel; ?></span>
									</a>
								</div>
							<?php endif; ?>
							<div class="col-auto langs-col">
								<div class="langs-wrap">
									<?php site_languages(); ?>
								</div>
							</div>
						</div>
					</div>
					<?php if ($logo = opt('logo')) : ?>
						<div class="col-auto header-logo-col">
							<a href="/" class="logo">
								<img src="<?= $logo['url'] ?>" alt="logo">
							</a>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</header>

<section class="pop-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-9 col-12 d-flex justify-content-center">
				<div class="float-form d-flex flex-column align-items-center">
					<div class="form-wrapper-pop">
						<span class="close-form">
							x
						</span>
						<?php if ($f_title = opt('pop_form_title')) : ?>
							<h2 class="form-title"><?= $f_title; ?></h2>
						<?php endif;
						if ($f_subtitle = opt('pop_form_subtitle')) : ?>
							<p class="form-subtitle"><?= $f_subtitle; ?></p>
						<?php endif;
						getForm('108'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="pop-form-search">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-9 col-12 d-flex justify-content-center">
				<div class="float-form-search d-flex flex-column align-items-center">
					<div class="form-wrapper-pop">
						<span class="close-form-search">
							x
						</span>
						<?php if ($title_search = opt('search_title')) : ?>
							<h2 class="base-title text-center mb-3"><?= $title_search; ?></h2>
						<?php endif;
						get_search_form(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<div class="triggers-col">
	<?php if ($whatsapp = opt('whatsapp')) : ?>
		<a href=https://api.whatsapp.com/send?phone="<?= $whatsapp; ?>" class="trigger-item">
			<img src="<?= ICONS ?>fix-whatsapp.png" alt="whatsapp">
		</a>
	<?php endif;
	if ($facebook = opt('facebook')) : ?>
		<a href="<?= $facebook; ?>" class="trigger-item">
			<img src="<?= ICONS ?>fix-facebook.png" alt="facebook">
		</a>
	<?php endif; ?>
	<div class="trigger-item pop-trigger">
		<?= lang_text(['he' => 'צור קשר', 'en' => 'Contact'], 'he'); ?>
	</div>
	<div class="trigger-item search-trigger">
		<img src="<?= ICONS ?>fix-search.png" alt="search-trigger">
	</div>
</div>
