<?php
get_header();
$fields = get_fields();
?>
<div class="post-output-block page-body pb-5">
	<div class="container pt-5">
		<?php
		$s = get_search_query();
		$args_1 = array(
			'post_type' => 'post',
			's' => $s
		);
		$args_2 = array(
			'post_type' => 'product',
			's' => $s
		);
		$the_query_1 = new WP_Query( $args_1 );
		$the_query_2 = new WP_Query( $args_2 );
		if ( $the_query_1->have_posts() ) { ?>
		<h4 class="base-title my-3"><?= esc_html__('תוצאות חיפוש עבור:','leos');?><?= get_query_var('s') ?></h4>
		<div class="row justify-content-center align-items-stretch">
			<?php while ( $the_query_1->have_posts() ) { $the_query_1->the_post();
				$link = get_the_permalink(); ?>
				<div class="col-xl-3 col-md-6 col-sm-10 col-12 col-post">
					<div class="post-card">
						<div class="post-img"<?php if (has_post_thumbnail()) : ?>
							style="background-image: url('<?= postThumb(); ?>')" <?php endif; ?>>
							<a class="post-card-content" href="<?= $link; ?>">
								<span class="post-card-title"><?php the_title(); ?></span>
								<span class="base-text">
									<?= text_preview(get_the_content(), 10); ?>
								</span>
							</a>
						</div>
						<a href="<?= $link; ?>" class="post-link">
							<?= lang_text(['he' => 'קראו עוד', 'en' => 'Read more'], 'he'); ?>
						</a>
					</div>
				</div>
			<?php }
			} ?>
		</div>
		<div class="row justify-content-center align-items-stretch">
			<?php if ( $the_query_2->have_posts() ) {
				while ( $the_query_2->have_posts() ) { $the_query_2->the_post(); ?>
					<div class="col-lg-3 col-md-6 col-sm-10 col-12 mb-4">
						<?php
						$post_object = get_post( get_the_ID());

						setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

						wc_get_template_part( 'content', 'product' );
						?>
					</div>
				<?php }
			} ?>
		</div>
			<?php if (!$the_query_1->have_posts() && !$the_query_2->have_posts()) : ?>
				<div class="col-12 pt-5">
					<h4 class="base-title">
						<?= esc_html__('שום דבר לא נמצא','leos'); ?>
					</h4>
				</div>
				<div class="alert alert-info text-center mt-5">
					<p><?= esc_html__('מצטערים, אך שום דבר לא תאם את קריטריוני החיפוש שלך. אנא נסה שוב עם מילות מפתח שונות.','leos'); ?></p>
				</div>
			<?php endif; ?>
		</div>
	</div>
	</div>
</div>
<?php get_footer(); ?>
