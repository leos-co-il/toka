<?php

the_post();
get_header();
$fields = get_fields();

?>
<article class="page-body cart-page-body">
	<div class="container">
		<div class="row">
			<div class="col-12 base-output">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
