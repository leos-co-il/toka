<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;
global $wp_query;
if (is_shop()) {
	$query = wc_get_page_id('shop');
} else {
	$query_curr = get_queried_object();
	$query = $query_curr;
}
get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action('woocommerce_before_main_content');

global $wp_query;
?>
	<div class="container mt-5 pb-4">
		<div class="row justify-content-center align-items-center">
			<div class="col-auto d-flex justify-content-center">
				<h1 class="base-shop-title"><?php woocommerce_page_title(); ?></h1>
			</div>
		</div>
		<div class="row justify-content-center mb-3">
			<div class="col-auto">
				<div class="base-output text-center">
					<?php
					/**
					 * Hook: woocommerce_archive_description.
					 *
					 * @hooked woocommerce_taxonomy_archive_description - 10
					 * @hooked woocommerce_product_archive_description - 10
					 */
					do_action( 'woocommerce_archive_description' );
					?>
				</div>
			</div>
		</div>
		<div class="row pt-3">
			<div class="col-xl-3 col-lg-4">
				<?php


				/**
				 * Hook: woocommerce_sidebar.
				 *
				 * @hooked woocommerce_get_sidebar - 10
				 */
				do_action( 'woocommerce_sidebar' );

				?>
			</div>
			<div class="col-xl-9 col-lg-8">
				<div class="woo-notice">
					<?php woocommerce_output_all_notices() ?>
				</div>
				<?php
				if ( woocommerce_product_loop() ) {
					woocommerce_product_loop_start();
					echo '<div class="row query-product-list put-here-prods">';
					if ( wc_get_loop_prop( 'total' ) ) {
						while ( have_posts() ) {
							the_post();

							/**
							 * Hook: woocommerce_shop_loop.
							 */
							do_action( 'woocommerce_shop_loop' );

							echo '<div class="col-xl-4 col-sm-6 col-12 mb-4 product-item-col show-filter">';
							wc_get_template_part( 'content', 'product' );
							echo '</div>';
						}
					}
					echo '<div>';
					woocommerce_product_loop_end();

					/**
					 * Hook: woocommerce_after_shop_loop.
					 *
					 * @hooked woocommerce_pagination - 10
					 */
					do_action( 'woocommerce_after_shop_loop' );
				} else {
					/**
					 * Hook: woocommerce_no_products_found.
					 *
					 * @hooked wc_no_products_found - 10
					 */
					do_action( 'woocommerce_no_products_found' );
				}
				 ?>
			</div>
		</div>
	</div>
<?php

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );
get_footer( 'shop' );
