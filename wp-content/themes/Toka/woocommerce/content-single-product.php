<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
$post_link = get_the_permalink();
$post_id = $product->get_id();
$category = get_the_terms($post_id, 'product_cat');
$product_thumb = wp_get_attachment_image_url( get_post_thumbnail_id($post_id), 'full' );;
$product_gallery = $product->get_gallery_image_ids();
$product_gallery_images = [];
if($product_gallery){
	foreach ($product_gallery as $_item){
		$product_gallery_images[] = wp_get_attachment_image_url($_item, 'large', '');
	}
}
$categories = wp_get_object_terms($post_id, 'product_cat', ['fields' => 'ids']);
$samePosts = get_posts([
		'posts_per_page' => 4,
		'post_type' => 'product',
		'post__not_in' => array($post_id),
		'suppress_filters' => false,
		'tax_query' => [
				[
						'taxonomy' => 'product_cat',
						'field' => 'term_id',
						'terms' => $categories,
				],
		],
]);
if (!$samePosts) {
	$samePosts = get_posts([
			'posts_per_page' => 4,
			'post_type' => 'product',
			'suppress_filters' => false,
			'post__not_in' => array($post_id)
	]);
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
	<div class="container pt-4 pb-5">
		<div class="row justify-content-start row-prod-title-mob">
			<div class="col">
				<h2 class="product-main-title mb-3"><?= $product->get_title(); ?></h2>
			</div>
		</div>
		<div class="row justify-content-between align-items-start">
			<div class="col-xl-5 col-lg-6 col-12 product-page-info-col">
				<h1 class="product-main-title desctop-title"><?= $product->get_title(); ?></h1>
				<h3 class="product-sku-main">
					<?= lang_text(['he' => ' מק”ט:', 'en' => 'Articul: '], 'he').$product->get_sku(); ?>
				</h3>
				<div class="summary entry-summary">
					<?php
					/**
					 * Hook: woocommerce_single_product_summary.
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 * @hooked WC_Structured_Data::generate_product_data() - 60
					 */
					do_action( 'woocommerce_single_product_summary' );
					?>
				</div>
				<?php if ($logo = get_field('brand_logo', $post_id)) : ?>
					<div class="prod-logo">
						<img src="<?= $logo['url']; ?>" alt="brand-of-product">
					</div>
				<?php endif; ?>
			</div>
			<div class="col-lg-6 product-page-gal-col">
				<div class="gallery-slider-wrap">
					<div class="thumbs">
						<?php if($product_thumb): ?>
							<div class="p-1">
								<a class="thumb-item" href="<?= $product_thumb; ?>" data-lightbox="images-small">
									<img src="<?= $product_thumb; ?>" alt="product-img">
								</a>
							</div>
						<?php endif;
						foreach ($product_gallery_images as $img): ?>
							<div class="p-1">
								<a class="thumb-item" href="<?= $img; ?>" data-lightbox="images-small">
									<img src="<?= $img; ?>" alt="gallery-img">
								</a>
							</div>
						<?php endforeach; ?>
					</div>
					<div class="gallery-slider" dir="rtl">
						<?php if($product_thumb): ?>
							<div class="px-1">
								<a class="big-slider-item" style="background-image: url('<?= $product_thumb; ?>')"
								   href="<?= $product_thumb; ?>" data-lightbox="images">
									<span class="gallery-trigger">
										<img src="<?= ICONS ?>fix-search.png" alt="view-more">
									</span>
								</a>
							</div>
						<?php endif;
						foreach ($product_gallery_images as $img): ?>
							<div class="px-1">
								<a class="big-slider-item" style="background-image: url('<?= $img; ?>')"
								   href="<?= $img; ?>" data-lightbox="images">
									<span class="gallery-trigger">
										<img src="<?= ICONS ?>fix-search.png" alt="view-more">
									</span>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="prod-form-wrap" data-id="<?= $post_id; ?>" data-link="<?= $post_link; ?>">
					<?php if ($title_f = opt('prod_form_title')) : ?>
						<h3 class="product-sku-main font-weight-bold mb-2">
							<?= $title_f; ?>
						</h3>
					<?php endif;
					getForm('109'); ?>
				</div>
				<div class="socials-share">
					<span class="base-text share-text mx-2">
						<?= lang_text(['he' => 'שתפו את המוצר', 'en' => 'Share product'], 'he'); ?>
					</span>
					<!--	WHATSAPP-->
					<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>" class="social-share-link">
						<img src="<?= ICONS ?>whatsapp.png">
					</a>
					<!--	MAIL-->
					<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
					   class="social-share-link">
						<img src="<?= ICONS ?>email.png">
					</a>
				</div>
			</div>
		</div>
	</div>
	<?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action( 'woocommerce_after_single_product_summary' );
	if ($samePosts) : ?>
		<section class="related-block py-5">
			<div class="related-title-wrap">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-auto">
							<?php if ($text = opt('same_prods_text')) : ?>
								<div class="base-output block-output text-center">
									<?= $text; ?>
								</div>
							<?php else : ?>
								<h2 class="block-title">
									<?= lang_text(['he' => 'מוצרים נוספים שעשויים לעניין אתכם/', 'en' => 'Other products that may interest you /'], 'he'); ?>
								</h2>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($samePosts as $_p){
						echo '<div class="col-lg-3 col-sm-6 col-12 mb-4">';
						setup_postdata($GLOBALS['post'] =& $_p);
						wc_get_template_part( 'content', 'product' );
						echo '</div>';
					}
					wp_reset_postdata();?>
				</div>
				<?php if ($cats_link = opt('cats_page')) : ?>
					<div class="row justify-content-center">
						<div class="col-auto">
							<a href="<?= $cats_link['url']; ?>" class="base-link">
								<?= lang_text(['he' => 'לכל הקטגוריות', 'en' => 'To all categories'], 'he'); ?>
							</a>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</section>
	<?php endif;
	?>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
